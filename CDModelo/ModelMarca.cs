﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using CDControlador;

namespace CDModelo
{
    public class ModelMarca
    {
        private ConMarca objMarca = new ConMarca();

        private int _codigoMarca;
        private string _nombreMarca;
        private int _EstadoMarca;

        public int codigoMarca
        {
            set { _codigoMarca = value; }
            get { return _codigoMarca; }
        }
        public string nombreMarca
        {
            set { _nombreMarca = value; }
            get { return _nombreMarca; }
        }
        public int estadoMarca
        {
            set { _EstadoMarca = value; }
            get { return _EstadoMarca; }
        }

        public MySqlDataReader ingresarP()
        {
            MySqlDataReader agregar;
            agregar = objMarca.agregarMarca(codigoMarca, nombreMarca, estadoMarca);

            return agregar;
        }
    }
}
