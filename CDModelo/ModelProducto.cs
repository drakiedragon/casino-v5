﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data;
using CDControlador;
using MySql.Data.MySqlClient;

namespace CDModelo
{
    public class ModelProducto
    {
        private ConProducto objProducto = new ConProducto();

        private int _codigoProducto;
        private string _nombreProducto;
        private int _precioProducto;
        private int _stockProducto;
        private int _stockMinimo;
        private int _idSubCategoria;
        private int _idMarca;
        private int _EstadoProducto;
       

        public int codigoProducto
        {
            set { _codigoProducto = value; }
            get { return _codigoProducto; }
        }
        public string nombreProducto
        {
            set { _nombreProducto = value; }
            get { return _nombreProducto; }
        }
        public int precioProducto
        {
            set { _precioProducto = value; }
            get { return _precioProducto; }
        }
        public int stockProducto
        {
            set { _stockProducto = value; }
            get { return _stockProducto; }
        }

        public int stockMinimo
        {
            set { _stockMinimo = value; }
            get { return _stockMinimo; }
        }


        public int idSubCategoria
        {
            set { _idSubCategoria = value; }
            get { return _idSubCategoria; }
        }
        public int idMarca
        {
            set { _idMarca = value; }
            get { return _idMarca; }
        }

        public int EstadoProducto
        {
            set { _EstadoProducto = value; }
            get { return _EstadoProducto; }
        }

        public MySqlDataReader IgresarProductos()
        {
            MySqlDataReader agregar;
            agregar = objProducto.agregarProducto(codigoProducto,nombreProducto,precioProducto,stockProducto,stockMinimo,idSubCategoria,EstadoProducto, idMarca);

            return agregar;
        }

        public MySqlDataReader ListarProductoCodigo(int codigoProducto1)
        {
            MySqlDataReader mostrar;
            mostrar = objProducto.ListarProductoCodigo(codigoProducto1);

            return mostrar;
        }

        public MySqlDataReader solicitarProductoAeditar()
        {
            MySqlDataReader mostrar;
            mostrar = objProducto.SolicitarProductoAEditar(codigoProducto);

            return mostrar;
        }

        public MySqlDataReader solicitarNombreMarcaAndSubcateAndCateg()
        {
            MySqlDataReader mostrar;
            mostrar = objProducto.PedirNombreSubYCaProductoAeditar(codigoProducto);

            return mostrar;
        }

        public MySqlDataReader solicitudCodigoCategoria(string nombrecategoria)
        {
            MySqlDataReader mostrar;
            mostrar = objProducto.SolicitarIDCategoriaProductoPorNombre(nombrecategoria);

            return mostrar;
        }

        public MySqlDataReader solicitudCodigoMarca(string nombrecategoria)
        {
            MySqlDataReader mostrar;
            mostrar = objProducto.SolicitarIDMarcaProductoPorNombre(nombrecategoria);

            return mostrar;
        }

        public MySqlDataReader solicitudCodigoSubcategoria(string nombreSubcategoria)
        {
            MySqlDataReader mostrar;
            mostrar = objProducto.SolicitarIDSubCategoriaProductoPorNombre(nombreSubcategoria);

            return mostrar;
        }

        public MySqlDataReader solicitarUnidadesMedidaP()
        {
            MySqlDataReader mostrar;
            mostrar = objProducto.SolicitarUnidadesDeMedidaProducto();

            return mostrar;
        }

        public MySqlDataReader solicitarCategoriasP()
        {
            MySqlDataReader mostrar;
            mostrar = objProducto.SolicitarCategoriasDeProducto();

            return mostrar;
        }

        public MySqlDataReader solicitarSubCategoriasP(int codigo_categoria)
        {
            MySqlDataReader mostrar;
            mostrar = objProducto.SolicitarSubCategoriasDeProducto(codigo_categoria);

            return mostrar;
        }

        public MySqlDataReader solicitarMarcasP()
        {
            MySqlDataReader mostrar;
            mostrar = objProducto.SolicitarMarcasP();

            return mostrar;
        }

        public MySqlDataReader iniciarUpdate()
        {
            MySqlDataReader ingresar;
            ingresar = objProducto.ActualizarProducto(nombreProducto, precioProducto, idSubCategoria, idMarca, codigoProducto, stockMinimo);

            return ingresar;
        }

        public MySqlDataReader MostrarProductoAEliminar()
        {
            MySqlDataReader mostrarE;
            mostrarE = objProducto.MostrarProductoPorCodigo(codigoProducto);

            return mostrarE;
        }

        public MySqlDataReader eliminarProducto()
        {
            MySqlDataReader mostrarE;
            mostrarE = objProducto.EliminarProductoPorCodigo(codigoProducto);

            return mostrarE;
        }

        public MySqlDataReader estadoProducto()
        {
            MySqlDataReader mostrarEP;
            mostrarEP = objProducto.SolicitarEstadoProducto(codigoProducto);

            return mostrarEP;
        }

        /*public MySqlDataReader Mostrar_datos_usuario()
        {
            MySqlDataReader mostrar_nombre;
            mostrar_nombre = objDato.Mostrar_datos_usuario(Nombre_usuario, email, Telefono);
            return mostrar_nombre;

        }
        */
    }
}