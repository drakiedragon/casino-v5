﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using CDControlador;
using CDModelo;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VistaWPF
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void agregarProducto_Click(object sender, RoutedEventArgs e)
        {
            ContentGridDashboard.Children.Clear();
            AgregarProducto agregarProductoControl = new AgregarProducto();
            ContentGridDashboard.Children.Add(agregarProductoControl);
        }

        private void editarProducto_Click(object sender, RoutedEventArgs e)
        {
            ContentGridDashboard.Children.Clear();
            EditarProducto editarProductoControl = new EditarProducto();
            ContentGridDashboard.Children.Add(editarProductoControl);
        }


        private void buscarProducto_Click(object sender, RoutedEventArgs e)
        {
            ContentGridDashboard.Children.Clear();
            BuscarProducto buscarProductoControl = new BuscarProducto();
            ContentGridDashboard.Children.Add(buscarProductoControl);
        }

        private void agregarMarca_Click(object sender, RoutedEventArgs e)
        {
            ContentGridDashboard.Children.Clear();
            AgregarMarcas agregarMarcaControl = new AgregarMarcas();
            ContentGridDashboard.Children.Add(agregarMarcaControl);
        }

    }
}
