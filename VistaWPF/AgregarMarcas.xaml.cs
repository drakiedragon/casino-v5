﻿using CDModelo;
using MySql.Data.MySqlClient;
using System;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VistaWPF
{
    /// <summary>
    /// Lógica de interacción para AgregarMarca.xaml
    /// </summary>
    public partial class AgregarMarcas : UserControl
    {
        public AgregarMarcas()
        {
            InitializeComponent();
        }

        private void Btn_Ingresar_Marca(object sender, RoutedEventArgs e)
        {
            try
            {
                if (InputNombreMarca.Text != "" && InputNombreMarca.Text != null)
                {

                    ModelMarca objMarca = new ModelMarca();
                    MySqlDataReader ingresarMarca;




                    objMarca.nombreMarca = InputNombreMarca.Text;

                    ingresarMarca = objMarca.ingresarP();

                    ingresarMarca.Close();
                    
                    MessageBox.Show(messageBoxText: "Nueva Marca Registrada", caption: "Registro Marca", button: MessageBoxButton.OK, icon: MessageBoxImage.Information);
                    InputNombreMarca.Text = "";
                }
                else
                {
                    MessageBox.Show(messageBoxText: "Hay Campos Vacios, Rellene o Seleccione los Campos Restantes e intente nuevamente.", caption: "Error", button: MessageBoxButton.OK, icon: MessageBoxImage.Exclamation);
                    InputNombreMarca.Text = "";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(messageBoxText: "Error!: " + ex, caption: "Error de ingreso", button: MessageBoxButton.OK, icon: MessageBoxImage.Error);
                InputNombreMarca.Text = "";
            }

        }

    }
}
