﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using CDControlador;
using CDModelo;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace VistaWPF
{
    /// <summary>
    /// Lógica de interacción para EditarProducto.xaml
    /// </summary>
    public partial class EditarProducto : UserControl
    {

        public EditarProducto()
        {

            InitializeComponent();

        }
        public BuscarProducto listarPadre;

        public string nombreSubcategoria;
        public int codigoProducto;
        public int DesignationName
        {
            get
            {
                return codigoProducto;
            }
            set
            {
                codigoProducto = value;
            }
        }


        public static string Mid(string param, int startIndex, int length)

        {

            string result = param.Substring(startIndex, length);

            return result;

        }

        public void cargar_producto()
        {
            string nombreCategoria = "";
            string nombreMarca = "";
            string codigoCategoria;

            ModelProducto objProducto = new ModelProducto();
            MySqlDataReader obtenerProducto;
            MySqlDataReader categoriaP;
            MySqlDataReader marcaP;
            MySqlDataReader subcategoriaP;


            comboMarcaProducto.Items.Clear();
            comboCategoriaProducto.Items.Clear();
            comboSubcategoriaProducto.Items.Clear();
            objProducto.codigoProducto = codigoProducto;



            obtenerProducto = objProducto.solicitarProductoAeditar();
            while (obtenerProducto.Read() == true)
            {
                try
                {
                    codigoProductoText.Text = "CÓDIGO DEL PRODUCTO: " + codigoProducto;
                    inputNombreProducto.Text = obtenerProducto.GetString(0);
                    inputPrecioProducto.Text = obtenerProducto.GetString(1);
                    nombreSubcategoria = obtenerProducto.GetString(2);
                    nombreMarca = obtenerProducto.GetString(3);
                    nombreCategoria = obtenerProducto.GetString(4);
                    inputStockMinimo.Text = obtenerProducto.GetString(5);

                }
                catch (Exception ex)
                {

                    MessageBox.Show(messageBoxText: Convert.ToString(ex), caption: "", button: MessageBoxButton.OK, icon: MessageBoxImage.Exclamation);
                }


            }

            obtenerProducto.Close();
            subcategoriaP = objProducto.solicitudCodigoCategoria(nombreCategoria);
            subcategoriaP.Read();
            codigoCategoria = subcategoriaP.GetString(0);
            subcategoriaP.Close();
            subcategoriaP = objProducto.solicitarSubCategoriasP(Convert.ToInt32(codigoCategoria));
            while (subcategoriaP.Read())
            {
                comboSubcategoriaProducto.Items.Add(subcategoriaP.GetString(0));
            }
            subcategoriaP.Close();

            categoriaP = objProducto.solicitarCategoriasP();
            while (categoriaP.Read() == true)
            {
                try
                {
                    comboCategoriaProducto.Items.Add(categoriaP.GetString(0));

                }
                catch (Exception ex)
                {
                    MessageBox.Show(messageBoxText: Convert.ToString(ex), caption: "", button: MessageBoxButton.OK, icon: MessageBoxImage.Exclamation);
                }
            }


            categoriaP.Close();
            marcaP = objProducto.solicitarMarcasP();

            while (marcaP.Read() == true)
            {
                try
                {

                    comboMarcaProducto.Items.Add(marcaP.GetString(0));
                }
                catch (Exception ex)
                {
                    MessageBox.Show(messageBoxText: Convert.ToString(ex), caption: "", button: MessageBoxButton.OK, icon: MessageBoxImage.Exclamation);
                }

            }
            marcaP.Close();

            comboMarcaProducto.SelectedItem = nombreMarca;
            comboSubcategoriaProducto.Text = nombreSubcategoria;
            comboCategoriaProducto.SelectedItem = nombreCategoria;

        }







        private void comboCategoriaProducto_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            comboSubcategoriaProducto.Items.Clear();
            ModelProducto objProducto = new ModelProducto();
            string codigo_categoria;
            MySqlDataReader CategoriaP;
            MySqlDataReader subCategoriaP;

            if (comboCategoriaProducto.Text != null && comboCategoriaProducto.Text != "Categoria")
            {
                CategoriaP = objProducto.solicitudCodigoCategoria(comboCategoriaProducto.SelectedItem.ToString());
                CategoriaP.Read();
                codigo_categoria = CategoriaP.GetString(0);

                CategoriaP.Close();
                subCategoriaP = objProducto.solicitarSubCategoriasP(Convert.ToInt32(codigo_categoria));
                while (subCategoriaP.Read() == true)
                {
                    try
                    {
                        comboSubcategoriaProducto.Items.Add(subCategoriaP.GetString(0));
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(messageBoxText: Convert.ToString(ex), caption: "", button: MessageBoxButton.OK, icon: MessageBoxImage.Exclamation);
                    }
                }
                comboSubcategoriaProducto.IsEnabled = true;
                comboSubcategoriaProducto.SelectedItem = nombreSubcategoria;
            }
            else
            {
                MessageBox.Show(messageBoxText: "Ah dejado el Campo Vacio, esta accion no es recomendable, por favor seleccion un item de la lista.", caption: "", button: MessageBoxButton.OK, icon: MessageBoxImage.Exclamation);

            }

        }

        private void Btn_Actualizar_Producto(object sender, RoutedEventArgs e)
        {
            try
            {


                if (codigoProductoText.Text != "" && inputNombreProducto.Text != "" && inputPrecioProducto.Text != "" && comboSubcategoriaProducto.Text != "" && comboSubcategoriaProducto.Text != "Tipo De Producto" && comboSubcategoriaProducto.SelectedItem != null && comboCategoriaProducto.Text != "" && comboCategoriaProducto.Text != "Categoria" && comboCategoriaProducto.SelectedItem != null && comboMarcaProducto.Text != "" && comboMarcaProducto.Text != "Marca" && comboMarcaProducto.SelectedItem != null)
                {
                    string CodigoSubcategoria;
                    string CodigoMarca;
                    ModelProducto objProducto = new ModelProducto();
                    MySqlDataReader actualizarProducto;
                    MySqlDataReader solicitarProductoParaConfirmar;
                    MySqlDataReader codigoSubcategoriaP;
                    MySqlDataReader codigoMarcaP;

                    //Se solicita el codigo de subcategoria
                    codigoSubcategoriaP = objProducto.solicitudCodigoSubcategoria(comboSubcategoriaProducto.SelectedItem.ToString());
                    codigoSubcategoriaP.Read();
                    CodigoSubcategoria = codigoSubcategoriaP.GetString(0);
                    codigoSubcategoriaP.Close();

                    objProducto.codigoProducto = codigoProducto;
                    objProducto.nombreProducto = inputNombreProducto.Text;
                    objProducto.precioProducto = Convert.ToInt32(inputPrecioProducto.Text);
                    objProducto.idSubCategoria = Convert.ToInt32(CodigoSubcategoria);

                    //Se solicita el codigo de la marca

                    codigoMarcaP = objProducto.solicitudCodigoMarca(comboMarcaProducto.SelectedItem.ToString());
                    codigoMarcaP.Read();
                    CodigoMarca = codigoMarcaP.GetString(0);
                    codigoMarcaP.Close();

                    objProducto.idMarca = Convert.ToInt32(CodigoMarca);
                    objProducto.stockMinimo = Convert.ToInt32(inputStockMinimo.Text);
                    actualizarProducto = objProducto.iniciarUpdate();
                    actualizarProducto.Read();
                    actualizarProducto.Close();
                    solicitarProductoParaConfirmar = objProducto.solicitarProductoAeditar();
                    solicitarProductoParaConfirmar.Read();
                    MessageBox.Show(messageBoxText: "Actualización de Producto Exitosa!, " +
                        " Datos Actualizados: " +
                        " ** Nombre Producto: " + solicitarProductoParaConfirmar.GetString(0) +
                        " ** Precio Producto:  $ " + solicitarProductoParaConfirmar.GetString(1) +
                        " ** Tipo De Producto: " + solicitarProductoParaConfirmar.GetString(2) +
                        " ** Marca de Producto: " + solicitarProductoParaConfirmar.GetString(3) +
                        " ** stock Minimo de Producto: " + solicitarProductoParaConfirmar.GetString(5), caption: "", button: MessageBoxButton.OK, icon: MessageBoxImage.Information);
                    solicitarProductoParaConfirmar.Close();

                }
                else
                {
                    MessageBox.Show(messageBoxText: "Hay Campos Vacios, Rellene o Seleccione los Campos Restantes e intente nuevamente.", caption: "", button: MessageBoxButton.OK, icon: MessageBoxImage.Exclamation);
                    MainWindow main = new MainWindow();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(messageBoxText: "Error!: " + ex, caption: "Error de edición", button: MessageBoxButton.OK, icon: MessageBoxImage.Error);

            }

        }


        private void volverControlPadre(object sender, RoutedEventArgs e)
        {
            Grid asd = (Grid)this.Parent;
            asd.Children.Remove(this);
            if (listarPadre != null)
            {
                listarPadre.Visibility = Visibility.Visible;
            }

        }


    }

}