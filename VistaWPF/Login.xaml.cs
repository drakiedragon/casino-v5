﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MySql.Data;
using MySql.Data.MySqlClient;
using CDControlador;
using CDModelo;

namespace VistaWPF
{
    /// <summary>
    /// Lógica de interacción para Window1.xaml
    /// </summary>
    public partial class Login : Window
    {
        //Instanciando el dashboard
        

        public Login()
        {
            InitializeComponent();
        }

        private void iniciar_sesion_Click(object sender, RoutedEventArgs e)
        {
            ModelEmpleado objEmpleado = new ModelEmpleado();
            MySqlDataReader confirmarUsuario;
            objEmpleado.Nickname = inputAccountUser.Text;
            objEmpleado.Pass = inputPasswordUser.Password;
            confirmarUsuario = objEmpleado.IniciarSesion();

            try
            {
                if (inputAccountUser.Text != "" && inputPasswordUser.Password != "")
                {
                    if(confirmarUsuario.Read() == true)
                    {
                        MainWindow principal = new MainWindow();
                        this.Close();
                        principal.Show();
                        
                    }
                    else
                    {
                        MessageBox.Show(messageBoxText: "Usuario o Contraseña incorrectos, intente nuevamente", caption: "Error de inicio", button: MessageBoxButton.OK, icon: MessageBoxImage.Exclamation);

                    }
                }
                else
                {
                    MessageBox.Show(messageBoxText: "Campos Vacios, intente nuevamente", caption: "Error de inicio", button: MessageBoxButton.OK, icon: MessageBoxImage.Exclamation);

                }

            }
            catch(Exception ex)
            {
                MessageBox.Show(messageBoxText: "Erro al iniciar sesion: "+ex, caption: "Error de inicio", button: MessageBoxButton.OK, icon: MessageBoxImage.Error);

            }


        }
    }
}
